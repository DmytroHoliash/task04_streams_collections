package com.holiash.command;

public class CommandImpl implements Command {

  @Override
  public void execute(String msg) {
    System.out.println("using object of command class " + msg);
  }
}
