package com.holiash.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class CommandApp {

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    Map<String, Command> menu = initMenu();
    showMenu(menu.keySet());
    String choice;
    while (!(choice = scanner.nextLine().toLowerCase()).equals("exit")) {
      if (menu.containsKey(choice)) {
        System.out.println("Input message");
        menu.get(choice).execute(scanner.nextLine());
      } else {
        System.out.println("Wrong choice");
      }
    }
  }

  private static void show(String msg) {
    System.out.println("Using method reference " + msg);
  }

  private static void showMenu(Set<String> keys) {
    System.out.println("------Menu------");
    keys.forEach(k -> System.out.println("--" + k + "--"));
  }

  private static Map<String, Command> initMenu() {
    Map<String, Command> menu = new HashMap<>();
    menu.put("lambda", msg -> System.out.println("Using lambda expression " + msg));
    menu.put("method reference", CommandApp::show);
    menu.put("anonymous class", new Command() {
      @Override
      public void execute(String msg) {
        System.out.println("Using anonymous class " + msg);
      }
    });
    menu.put("object", new CommandImpl());
    return menu;
  }
}
