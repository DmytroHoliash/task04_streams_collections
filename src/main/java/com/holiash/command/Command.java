package com.holiash.command;

@FunctionalInterface
public interface Command {
  void execute(String msg);
}
