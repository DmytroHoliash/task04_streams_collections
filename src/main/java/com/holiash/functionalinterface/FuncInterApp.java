package com.holiash.functionalinterface;

public class FuncInterApp {

  public static void main(String[] args) {
    TernaryOperator max = (a, b, c) -> Math.max(Math.max(a, b), c);
    TernaryOperator average = (a, b, c) -> (a + b + c) / 3;
    System.out.println(max.operator(5, 10, -3));
    System.out.println(average.operator(5, 8, 9));
  }
}
