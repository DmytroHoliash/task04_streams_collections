package com.holiash.functionalinterface;

@FunctionalInterface
public interface TernaryOperator {
  int operator(int a, int b, int c);
}
