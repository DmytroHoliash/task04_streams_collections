package com.holiash.sterams;

import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;

public class StreamApp {

  private static final int SIZE = 10;
  private static final int MAX_VALUE = 100;

  public static void main(String[] args) {
    List<Integer> list = ListCreator.createOddRandomList(SIZE);
    System.out.println(list);
    Optional<Integer> max = list.stream().max(Integer::compareTo);
    Optional<Integer> min = list.stream().min(Integer::compareTo);
    double average = list.stream().mapToInt(Integer::intValue).average().getAsDouble();
    int sum = list.stream().mapToInt(Integer::intValue).sum();
    Optional<Integer> reduceSum = list.stream().reduce(Integer::sum);
    long biggerThanAvg = list.stream().filter(x -> x > average).count();
    System.out.println("max=" + max);
    System.out.println("min=" + min);
    System.out.println("sum=" + sum);
    System.out.println("reduce sum=" + reduceSum);
    System.out.println("average=" + average);
    System.out.println("num of bigger than avg=" + biggerThanAvg);
  }
}
