package com.holiash.sterams;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface ListCreator {

  static List<Integer> createRandomList(int size) {
    return Stream.generate(new Random()::nextInt)
        .limit(size)
        .collect(Collectors.toList());
  }

  static List<Integer> createRandomList(int size, int maxValue) {
    return Stream.generate(new Random()::nextInt)
        .filter(x -> x < maxValue && x > -maxValue)
        .limit(size)
        .collect(Collectors.toList());
  }

  static List<Integer> createOddRandomList(int size) {
    return Stream.generate(new Random()::nextInt)
        .filter(x -> x % 2 > 0)
        .limit(size)
        .collect(Collectors.toList());
  }

  static List<Integer> createRandomUniqueList(int size) {
    return Stream.generate(new Random()::nextInt)
        .distinct()
        .limit(size)
        .collect(Collectors.toList());
  }
}
