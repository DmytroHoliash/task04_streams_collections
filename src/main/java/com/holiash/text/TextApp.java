package com.holiash.text;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class TextApp {

  private static final Scanner scanner = new Scanner(System.in);

  public static void main(String[] args) {
    System.out.println("Write your text: ");
    String text = scanner.nextLine();
    info(text);
  }

  public static void info(String text) {
    text = text.replaceAll("[.,!\\-:;?]", "");
    text = text.replaceAll("\\s+", " ");
    Map<String, Long> wordFrequency = Arrays.stream(text.split(" "))
        .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
    System.out.println("Word count: \n" + wordFrequency);
    long numberOfUniqueWords = wordFrequency.entrySet().stream()
        .filter(en -> en.getValue() == 1).count();
    System.out.println("Number of unique words: " + numberOfUniqueWords);
    List<String> sortedUniqueWords = wordFrequency.entrySet().stream()
        .filter(en -> en.getValue() == 1)
        .map(Entry::getKey)
        .sorted()
        .collect(Collectors.toList());
    System.out.println("Sorted unique words: ");
    sortedUniqueWords.forEach(w -> System.out.print(" " + w));
    char[] chars = text.toCharArray();
    Map<Character, Long> characterOccurrences = IntStream.range(0, chars.length)
        .mapToObj(i -> chars[i])
        .filter(Character::isLowerCase)
        .collect(Collectors.groupingBy(ch -> ch, Collectors.counting()));
    System.out.println("Char frequency: \n" + characterOccurrences);
  }
}
